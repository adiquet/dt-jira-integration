import mock
import pytest # noqa: ignore

from datatheorem_api_client import ResultsAPIClient
from datatheorem_api_client.results_api.jira_integration_config import JiraExportEnum

from dt_jira_integration.client import JipyApiClient
from dt_jira_integration.client import JiraIntegrationConfiguration
from dt_jira_integration.sync_with_jira_action import CustomerJiraSyncAction
from .factories import MobileAppFactory
from .factories import SecurityFindingFactory
from .factories import SecurityFindingTargetFactory
from .factories import JiraIntegrationConfigurationFactory

@mock.patch('dt_jira_integration.sync_with_jira_action.all_jira_integration_configs')
@mock.patch('dt_jira_integration.sync_with_jira_action.all_security_finding_targets')
@mock.patch('dt_jira_integration.sync_with_jira_action.all_security_findings')
@mock.patch('dt_jira_integration.sync_with_jira_action.all_mobile_apps')
def test_no_new_security_finding_targets(mock_all_mobile_apps: mock.Mock,
                                         mock_all_security_findings: mock.Mock,
                                         mock_all_security_finding_targets: mock.Mock,
                                         mock_all_jira_integration_configs: mock.Mock,
                                         config: JiraIntegrationConfiguration,
                                         client: JipyApiClient):
    # Given there are no security targets when fetching them
    mock_all_security_finding_targets.return_value = []
    mock_all_security_findings.return_value = []
    mock_all_mobile_apps.return_value = []
    mock_all_jira_integration_configs.return_value = []

    # When we start checking if we should sync
    mock_action = mock.Mock()
    with mock.patch('dt_jira_integration.sync_with_jira_action.FindingTargetJiraSyncAction', return_value=mock_action):
        action = CustomerJiraSyncAction('api_key', config)
        action.perform()

    # Then the sync action process is not called
    assert mock_action.call_count == 0

'''
@mock.patch('dt_jira_integration.sync_with_jira_action.all_security_finding_targets')
@mock.patch('dt_jira_integration.sync_with_jira_action.all_security_findings')
@mock.patch('dt_jira_integration.sync_with_jira_action.all_mobile_apps')
def test_each_target_uses_its_appropriate_jira_integration_config(mock_all_mobile_apps: mock.Mock,
                                                                  mock_all_security_findings: mock.Mock,
                                                                  mock_all_security_finding_targets: mock.Mock,
                                                                  config: JiraIntegrationConfiguration,
                                                                  results_client: ResultsAPIClient,
                                                                  client: JipyApiClient):
    # Given some apps that have security targets
    results_client.list_security_finding_targets.return_value = ([], None)
    api_key = 'secret_key'

    all_mobile_apps = [MobileAppFactory() for _ in range(3)]
    mock_all_mobile_apps.return_value = all_mobile_apps

    all_security_findings = [SecurityFindingFactory(mobile_app_id=app.id) for app in all_mobile_apps]
    mock_all_security_findings.return_value = all_security_findings

    mock_all_security_finding_targets.return_value = [
        SecurityFindingTargetFactory(mobile_app_id=app.id, security_finding_id=finding.id)
        for app, finding in zip(all_mobile_apps, all_security_findings)
    ]

    # And every App has a config that it relates to, except for one that uses the global config
    list_jira_integrations = [
        JiraIntegrationConfigurationFactory(id=app.id)
        for app in all_mobile_apps[:-1]
    ]
    list_jira_integrations.append(JiraIntegrationConfigurationFactory(id=9))
    results_client.list_jira_integration_configs.return_value = list_jira_integrations

    mock_action = mock.Mock()
    # When the syncing process begins
    with mock.patch('dt_jira_integration.sync_with_jira_action.FindingTargetJiraSyncAction', return_value=mock_action):
        with mock.patch('dt_jira_integration.sync_with_jira_action.JipyApiClient', return_value=client):
            jira_sync_action = CustomerJiraSyncAction(api_key, config)
            jira_sync_action.results_client = results_client
            jira_sync_action.perform()

    # Then the api was only called once
    assert mock_action.call_count == 3
'''

@mock.patch('dt_jira_integration.sync_with_jira_action.all_jira_integration_configs')
@mock.patch('dt_jira_integration.sync_with_jira_action.all_security_finding_targets')
@mock.patch('dt_jira_integration.sync_with_jira_action.all_security_findings')
@mock.patch('dt_jira_integration.sync_with_jira_action.all_mobile_apps')
def test_jira_global_config_export_filter_is_disabled(mock_all_mobile_apps: mock.Mock,
                                                      mock_all_security_findings: mock.Mock,
                                                      mock_all_security_finding_targets: mock.Mock,
                                                      mock_all_jira_integration_configs: mock.Mock,
                                                      config: JiraIntegrationConfiguration,
                                                      client: JipyApiClient):
    """Should not post/comment anything since config is disabled
    """
    # Given a jira config with integration disabled
    config.export_filter = JiraExportEnum.DISABLED
    mock_results_client = mock.MagicMock()

    mock_all_mobile_apps.return_value = [MobileAppFactory(id=90)]
    mock_all_security_findings.return_value = [SecurityFindingFactory(id=100)]
    mock_all_security_finding_targets.return_value = [SecurityFindingTargetFactory(security_finding_id=100, mobile_app_id=90) for _ in range(3)]
    mock_all_jira_integration_configs.return_value = []

    # When we start the action
    with mock.patch('dt_jira_integration.sync_with_jira_action.JipyApiClient', return_value=client):
        jira_sync_action = CustomerJiraSyncAction('key', config)
        jira_sync_action.results_client = mock_results_client
        jira_sync_action.perform()

    # Then we see that no calls to results api happened
    assert jira_sync_action.results_client.call_count == 0
