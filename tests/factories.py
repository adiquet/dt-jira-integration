import random
import string

import factory
from factory import fuzzy

from datatheorem_api_client import MobileApp
from datatheorem_api_client import SecurityFinding
from datatheorem_api_client import SecurityFindingTarget
from datatheorem_api_client import SeverityEnum
from dt_jira_integration.jira_configuration import JiraIntegrationConfiguration


def random_nullable_attribute():
    """Return either a value or None

    Currently it only returns string values.
    """
    should_return_none = random.choice([True, False])
    if should_return_none:
        return None
    attribute = ''.join(random.choices(string.ascii_letters, k=10))
    return attribute


class SecurityFindingFactory(factory.Factory):
    class Meta:
        model = SecurityFinding

    id = fuzzy.FuzzyInteger(5)
    title = fuzzy.FuzzyText()
    severity = fuzzy.FuzzyChoice(SeverityEnum)
    category = fuzzy.FuzzyText()
    exploitability = fuzzy.FuzzyText()
    description = fuzzy.FuzzyText()
    recommendation = fuzzy.FuzzyText()
    mobile_app_id = fuzzy.FuzzyInteger(5)
    importance_tags = fuzzy.FuzzyText()
    links = fuzzy.FuzzyText()
    portal_url = fuzzy.FuzzyText(prefix='http://www.', suffix='@fake.com')
    priority = fuzzy.FuzzyChoice(['P0', 'P1', 'P2', 'P3'])
    secure_code = fuzzy.FuzzyAttribute(random_nullable_attribute)


class SecurityFindingTargetFactory(factory.Factory):
    class Meta:
        model = SecurityFindingTarget

    id = fuzzy.FuzzyInteger(100)
    text = fuzzy.FuzzyText()
    current_status = fuzzy.FuzzyChoice(['OPEN', 'NEW', 'CLOSED_FIXED', 'CLOSED_RISK_ACCEPTED', 'CLOSED_COMPENSATING_CONTROL', 'CLOSED_ITEM_NOT_FOUND', 'OPEN_READY_TO_RESCAN'])
    current_status_date = fuzzy.FuzzyText()
    mobile_app_id = fuzzy.FuzzyInteger(100)
    security_finding_id = fuzzy.FuzzyText(chars=string.digits)
    portal_url = fuzzy.FuzzyText(prefix='http://www.', suffix='@fake.com')
    statuses = fuzzy.FuzzyText()
    links = fuzzy.FuzzyText()
    external_id = fuzzy.FuzzyAttribute(random_nullable_attribute)


class MobileAppFactory(factory.Factory):
    class Meta:
        model = MobileApp

    id = fuzzy.FuzzyInteger(100)
    name = fuzzy.FuzzyText()
    bundle_id = fuzzy.FuzzyText()
    subscription = fuzzy.FuzzyText()
    platform = fuzzy.FuzzyText()
    release_type = fuzzy.FuzzyChoice(['APP_STORE', 'PRE_PROD'])
    scan_status = fuzzy.FuzzyText()
    links = fuzzy.FuzzyText()
    portal_url = fuzzy.FuzzyText()
    store_url = fuzzy.FuzzyText()
    scanned_version = fuzzy.FuzzyText()
    scan_status_date = fuzzy.FuzzyText()
    results_last_updated = fuzzy.FuzzyText()


class JiraIntegrationConfigurationFactory(factory.Factory):
    class Meta:
        model = JiraIntegrationConfiguration

    id = fuzzy.FuzzyInteger(100)
    base_url = fuzzy.FuzzyText()
    username = fuzzy.FuzzyText()
    password = fuzzy.FuzzyText()
    type_of_issue_name = fuzzy.FuzzyText()
    project_key_or_id = fuzzy.FuzzyText()
    export_filter = fuzzy.FuzzyText()
    export_prod = fuzzy.FuzzyChoice([True, False])
    export_pre_prod = fuzzy.FuzzyChoice([True, False])

    severity_field_config = None
    static_fields = None
    dynamic_fields = None
