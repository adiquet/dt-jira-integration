import random
import string
from http import HTTPStatus
from collections import namedtuple

from jira import JIRAError  # type: ignore

import mock  # type: ignore
import pytest  # type: ignore

from dt_jira_integration.client import JipyApiClient
from dt_jira_integration.jira_configuration import JiraIntegrationConfiguration


@pytest.fixture
def client(config) -> JipyApiClient:
    # Patch, so we don't make network requests
    with mock.patch('jira.JIRA.__init__', lambda x, y, basic_auth, proxies, options: None):
        client = JipyApiClient(config)
    return client


def generate_base_issue():
    """Creates the base that's needed to create a jipy issue

    This should play out the same as the Resource instance we pass
    around from `jira.resource.Resource`
    """
    _id = ''.join(random.choices(string.digits, k=5))
    key = ''.join(random.choices(string.ascii_letters, k=3))
    summary = ''.join(random.choices(string.ascii_letters, k=20))
    description = ''.join(random.choices(string.ascii_letters, k=50))
    issue_name = ''.join(random.choices(string.ascii_lowercase, k=5))
    labels = ['Data-Theorem']

    IssueType = namedtuple('IssueType', 'name')
    issue_type = IssueType(issue_name)

    Fields = namedtuple('Fields', 'summary description issuetype labels')
    fields = Fields(summary, description, issue_type, labels)

    Base = namedtuple('Base', 'id key fields')
    issue = Base(_id, key, fields)

    return issue


def test_jipy_get_issue(client: JipyApiClient, config: JiraIntegrationConfiguration):
    # Give a mock for the jira library
    with mock.patch('jira.JIRA.search_issues') as mock_request:
        mock_response = mock.Mock(status_code=HTTPStatus.OK)
        random_issue_base = generate_base_issue()
        mock_response.return_value = [random_issue_base]
        mock_request.return_value = mock_response()

        # When we call `get_issue` we should get the specified issue
        random_issue_key = ''.join(random.choices(string.ascii_letters, k=5))
        response = client.get_issue(random_issue_key)

        # Then the api should be called with correct Jira query
        mock_request.assert_called_once_with(
            f'ID={random_issue_key} and project={config.project_key_or_id}')

        # And the results should be as expected
        assert 1 == mock_response.call_count
        assert 1 == mock_request.call_count
        assert response.id == random_issue_base.id
        assert response.key == random_issue_base.key


def test_jipy_raise_error_when_get_issue_returns_empty_list(client, config):
    # Give a mock for the jira library
    with mock.patch('jira.JIRA.search_issues') as mock_request:
        mock_response = mock.Mock(status_code=HTTPStatus.OK)
        mock_response.return_value = []
        mock_request.return_value = mock_response()

        # When we call `get_issue` we should get the specified issue
        random_issue_key = ''.join(random.choices(string.ascii_letters, k=5))
        with pytest.raises(JIRAError):
            client.get_issue(random_issue_key)

        # Then the api should be called with correct Jira query
        mock_request.assert_called_once_with(
            f'ID={random_issue_key} and project={config.project_key_or_id}')

        # And the results should be as expected
        assert 1 == mock_response.call_count
        assert 1 == mock_request.call_count


def test_jipy_create_comment(client, monkeypatch):
    # Give a mock for the jira library
    with mock.patch('jira.JIRA.add_comment') as mock_request:
        # We make a call to client
        random_issue = generate_base_issue()
        client.add_comment(random_issue, 'body')

        # And the results should be as expected
        assert random_issue.key in mock_request.call_args[0]
        assert 'body' in mock_request.call_args[0]
