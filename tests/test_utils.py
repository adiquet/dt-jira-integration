import pytest

from datatheorem_api_client import ReleaseTypeEnum
from datatheorem_api_client import PriorityEnum
from datatheorem_api_client.results_api.jira_integration_config import JiraExportEnum

from dt_jira_integration.utils import does_target_pass_export_filter
from dt_jira_integration.utils import does_target_pass_release_type_export

from .factories import SecurityFindingFactory
from .factories import MobileAppFactory


@pytest.mark.parametrize('finding,export_filter,result', [
    (SecurityFindingFactory(), 'DISABLED', False),
    (SecurityFindingFactory(priority=PriorityEnum.P0), JiraExportEnum.ALL_ISSUES, True),
    (SecurityFindingFactory(priority=PriorityEnum.P1), JiraExportEnum.ALL_ISSUES, True),
    (SecurityFindingFactory(priority=PriorityEnum.P1), JiraExportEnum.P1_ISSUES_AND_BLOCKERS, True),
    (SecurityFindingFactory(priority=PriorityEnum.P3), JiraExportEnum.P1_ISSUES_AND_BLOCKERS, False),
])
def test_export_filter(finding, export_filter, result):
    # Given a finding and an export filter type
    # When we check if the finding should be processed
    # Then we see the result
    assert result == does_target_pass_export_filter(finding, export_filter)


@pytest.mark.parametrize('mobile_app,allow_prod,allow_pre_prod,result', [
    (MobileAppFactory(release_type=ReleaseTypeEnum.APP_STORE), True, False, True),
    (MobileAppFactory(release_type=ReleaseTypeEnum.APP_STORE), False, True, False),
    (MobileAppFactory(release_type=ReleaseTypeEnum.PRE_PROD), True, False, False),
    (MobileAppFactory(release_type=ReleaseTypeEnum.PRE_PROD), False, True, True),
])
def test_release_type_export(mobile_app, allow_prod, allow_pre_prod, result):
    # Give an mobile app, and some boolean rules
    # When we check if it passes the release type check
    # Then we see the result
    assert result == does_target_pass_release_type_export(
        mobile_app,
        allow_prod,
        allow_pre_prod
    )
