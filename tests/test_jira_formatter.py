from datatheorem_api_client import PlatformEnum

from dt_jira_integration.jira_formatter import FindingTargetToJiraFormatter
from dt_jira_integration.jira_configuration import JiraIntegrationConfiguration

from .factories import SecurityFindingFactory
from .factories import SecurityFindingTargetFactory


def test_correct_formatting(config: JiraIntegrationConfiguration):
    # Given a finding and finding_target
    finding = SecurityFindingFactory()
    finding_target = SecurityFindingTargetFactory()

    # And formatter is created from finding and finding_target
    formatter = FindingTargetToJiraFormatter(config)
    issue_dict = formatter.get_create_issue_dict(finding_target, finding, 'name', PlatformEnum.ANDROID)

    # Then formatter has all the correct keys need
    assert all([key in ['project', 'labels', 'issuetype', 'summary', 'description', config.severity_field_config.field_id]
                for key in issue_dict.keys()])
