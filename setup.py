import sys

from setuptools import setup
from setuptools.command.test import test as TestCommand

from dt_jira_integration import __version__


class PyTest(TestCommand):
    """Test wrapper to be able to run pytest when doing `setup.py test`
    """
    def initialize_options(self):
        TestCommand.initialize_options(self)

    def run_tests(self):
        import pytest
        errno = pytest.main([])
        sys.exit(errno)


setup(
    name='dt_jira_integration',
    version=__version__,
    description='Export Data Theorem security issues into Jira bug tracker',
    packages=['dt_jira_integration'],
    install_requires=[
        'jira==2.0.0',
        'requests==2.18.4',
        'datatheorem_api_client==4.0.0',
        'PyYAML==3.13',
    ],
    dependency_links=[
        'git+https://bitbucket.com/datatheorem/dt-api-client.git#egg=datatheorem_api_client-4.0.0',
    ],
    tests_require=[
        'mock==2.0.0',
        'pytest==3.2.5',
        'factory-boy==2.9.2',
    ],
    cmdclass={'test': PyTest}
)
