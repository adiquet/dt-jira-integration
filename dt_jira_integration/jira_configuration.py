from typing import List
from typing import Optional

import yaml

from datatheorem_api_client import JiraIntegrationConfig
from datatheorem_api_client.results_api.jira_integration_config import SeverityFieldConfig
from datatheorem_api_client.results_api.jira_integration_config import StaticField
from datatheorem_api_client.results_api.jira_integration_config import DynamicField
from datatheorem_api_client.results_api.jira_integration_config import JiraExportEnum

GLOBAL_JIRA_INTEGRATION_ID = 9


class JiraIntegrationConfiguration(object):
    """Simple object to hold all Jira configurations."""

    def __init__(self,
                 id: int,
                 base_url: str,
                 username: str,
                 password: str,
                 type_of_issue_name: str,
                 export_filter: str,
                 export_pre_prod: bool,
                 export_prod: bool,
                 project_key_or_id: str,
                 severity_field_config: SeverityFieldConfig=None,
                 static_fields: List[StaticField]=None,
                 dynamic_fields: List[DynamicField]=None,
                 label: str='Data-Theorem') -> None:
        self.id = id
        self.base_url = base_url
        self.username = username
        self.password = password
        self.label = label
        self.export_filter = export_filter
        self.export_prod = export_prod
        self.export_pre_prod = export_pre_prod
        self.type_of_issue_name = type_of_issue_name
        self.project_key_or_id = project_key_or_id

        self.severity_field_config = severity_field_config
        self.static_fields = static_fields or []
        self.dynamic_fields = dynamic_fields or []

    @classmethod
    def from_dict(cls, response: dict) -> 'JiraIntegrationConfiguration':
        # Extract each field from the API response and return a JiraIntegrationConfiguration
        static_fields = [
            StaticField.from_dict(sf)
            for sf in response.get('static_fields', []) if sf is not None
        ]
        dynamic_fields = [
            DynamicField.from_dict(dynamic)
            for dynamic in response.get('dynamic_fields', []) if dynamic is not None
        ]
        if 'severity_field_config' in response and response['severity_field_config'] is not None:
            severity: Optional[SeverityFieldConfig] = SeverityFieldConfig.from_dict(response['severity_field_config'])
        else:
            severity = None

        # Default to exporting P1's and blockers only to avoid high volume of ticket creation
        export_filter = response.get('export_filter', 'P1_ISSUES_AND_BLOCKERS')
        export_filter = JiraExportEnum[export_filter]

        return cls(
            id=response.get('id', GLOBAL_JIRA_INTEGRATION_ID),
            base_url=response['base_url'],
            username=response['username'],
            password=response['password'],
            export_filter=export_filter,
            export_pre_prod=response['export_pre_prod'],
            export_prod=response['export_prod'],
            type_of_issue_name=response['type_of_issue_name'],
            project_key_or_id=response['project_key_or_id'],
            severity_field_config=severity,
            static_fields=static_fields,
            dynamic_fields=dynamic_fields,
        )

    @classmethod
    def from_yaml(cls, yaml_path: str) -> 'JiraIntegrationConfiguration':
        with open(yaml_path, 'r') as yaml_file:
            contents = yaml_file.read()

        # When we are loading from a yaml file, we will assume it is the global config
        jira_config: dict = yaml.load(contents)
        jira_config['id'] = GLOBAL_JIRA_INTEGRATION_ID

        return cls.from_dict(jira_config)

    @classmethod
    def from_dt_jira_config(cls, dt_jira_config: JiraIntegrationConfig) -> 'JiraIntegrationConfiguration':
        return cls(
            id=dt_jira_config.id,
            base_url=dt_jira_config.base_url,
            username=dt_jira_config.username,
            password=dt_jira_config.password,
            type_of_issue_name=dt_jira_config.type_of_issue_name,
            export_filter=dt_jira_config.export_filter,
            export_pre_prod=dt_jira_config.export_pre_prod,
            export_prod=dt_jira_config.export_prod,
            project_key_or_id=dt_jira_config.project_key_or_id,
            severity_field_config=dt_jira_config.severity_field_config,
            static_fields=dt_jira_config.static_fields,
            dynamic_fields=dt_jira_config.dynamic_fields,
        )
