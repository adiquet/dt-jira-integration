"""Simple Jira Client."""
from typing import Dict
from typing import Any

import jira

from dt_jira_integration.logger import jira_logger
from dt_jira_integration.jira_configuration import JiraIntegrationConfiguration


class JipyApiClient(object):
    """Simple Client for interacting with Jira."""

    def __init__(self,
                 config: JiraIntegrationConfiguration,
                 verify_jira_certificate: bool=True,
                 proxies: Dict=None,
                 **kwargs: Any) -> None:
        self._config = config
        self._jira = jira.JIRA(
            config.base_url,
            basic_auth=(config.username, config.password),
            proxies=proxies,
            options={'verify': verify_jira_certificate},
            **kwargs
        )

    def get_issue(self, issue_key_or_id: str) -> jira.Issue:
        """Return specific issue with key or id from configured project.

        Will throw JIRAError issue doesn't exist/can't find it
        """
        jira_logger.info(f'Retrieving issue with ID: {issue_key_or_id}')
        # Doing a JQL with "ID" can search using either key or id
        issues = self._jira.search_issues(f'ID={issue_key_or_id} and project={self._config.project_key_or_id}')
        if len(issues) == 0:
            raise jira.JIRAError(f'{issue_key_or_id} does not exist or out of project scope')
        issue = issues[0]
        return issue

    def add_comment(self, issue: jira.Issue, body: str) -> None:
        jira_logger.info(f'Adding comment to {issue.key} with comment being: {body}')
        self._jira.add_comment(issue.key, body)
