import os

from datatheorem_api_client import JiraIntegrationConfig
from datatheorem_api_client import ResultsAPIClient

from dt_jira_integration.jira_configuration import JiraIntegrationConfiguration


def get_results_api_key() -> str:
    """Returns the Results V2 API key.
    This should be set as an environment variable.
    Will raise error if not set
    """
    api_key = os.getenv('RESULTS_API_KEY')
    if not api_key:
        raise Exception('Please set environment variable `RESULTS_API_KEY`')
    return api_key


def get_jira_config(results_api_key: str=None) -> JiraIntegrationConfiguration:
    """Returns the Jira Config to use.
    It is retrieved via the Results V2 API
    """
    client = ResultsAPIClient(results_api_key or get_results_api_key())
    response: JiraIntegrationConfig = client.get_jira_integration_config()

    return JiraIntegrationConfiguration.from_dt_jira_config(response)
