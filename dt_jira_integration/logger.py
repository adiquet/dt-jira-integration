import os
import logging
import pathlib
from typing import Any
from logging import Formatter
from logging import FileHandler

jira_logger: Any
# Quick hack to get this to work on GAE, since GAE file system is READ-ONLY
if os.getenv('GOOGLE_CLOUD_PROJECT', None):
    jira_logger = logging
else:
    LOG_FORMAT = "%(asctime)s [%(levelname)s]: %(message)s in %(pathname)s:%(lineno)d"
    LOG_LEVEL = logging.INFO

    current_path = pathlib.Path(__file__).parent
    LOG_FILE_PATH = current_path / 'log.txt'

    jira_logger = logging.getLogger('jira.logger')
    jira_logger.setLevel(LOG_LEVEL)

    logger_file_handler = FileHandler(LOG_FILE_PATH)  # type: ignore
    logger_file_handler.setLevel(LOG_LEVEL)
    logger_file_handler.setFormatter(Formatter(LOG_FORMAT))

    jira_logger.addHandler(logger_file_handler)
