from typing import Any
from typing import Dict

from datatheorem_api_client import SecurityFinding
from datatheorem_api_client import SecurityFindingTarget
from datatheorem_api_client import PlatformEnum
from datatheorem_api_client.results_api.jira_integration_config import JiraDynamicValueEnum

from dt_jira_integration.jira_configuration import JiraIntegrationConfiguration
from dt_jira_integration.utils import get_severity_level


class FindingTargetToJiraFormatter:
    """This implements the formatting logic for exporting a DT target to a Jira ticket.
    """

    def __init__(self, integration_config: JiraIntegrationConfiguration) -> None:
        # For formatting, we only need to keep the following
        self._project_id = integration_config.project_key_or_id
        self._label = integration_config.label
        self._type_issue_name = integration_config.type_of_issue_name
        self._severity_field = integration_config.severity_field_config
        self._constant_custom_fields = integration_config.static_fields
        self._dynamic_custom_fields = integration_config.dynamic_fields

    def get_create_issue_dict(
            self,
            finding_target: SecurityFindingTarget,
            finding: SecurityFinding,
            mobile_app_name: str,
            mobile_platform: PlatformEnum
    ) -> Dict[str, Any]:
        """Return a dictionary with all the arguments ready to be passed to JIRA.create_issue().
        """
        # First generate the content of the ticket
        issue_dict: Dict[str, Any] = {
            'project': self._project_id,  # {'key': self._project_id},
            'labels': [self._label],
            'issuetype': self._type_issue_name,  # {'name': self._type_issue_name},
            'summary': f'[{mobile_platform.name}] {mobile_app_name}: {finding.title}',
        }

        description = (f'h3. Affected Component\n{finding_target.text}\n'
                       f'h3. Description\n{finding.description}\n'
                       f'h3. Recommendation\n{finding.recommendation}\n\n')

        if finding.secure_code:
            description += f'h3. Secure Code\n{finding.secure_code}\n'

        # Since we have enforced that all code blocks use 3 back-ticks (```),
        # it's safe to assume that we can replace all instances of those with Jira's version of code blocks
        description = description.replace('```', '{code}')

        issue_dict['description'] = description

        # Then, add the custom fields
        for field in self._constant_custom_fields:
            # TODO(Gopar): Check here for diff ways of saving values
            # When the value is pure digits, we assume it's associate to a specific value on Jira's end so we send
            # it in a dict with the id. Otherwise we assume it's just plain text
            value = field.field_value if not field.field_value.isdigit() else {'id': field.field_value}
            issue_dict[field.field_id] = value

        for field in self._dynamic_custom_fields:  # type: ignore
            if field.value == JiraDynamicValueEnum.FINDING_PORTAL_URL:
                issue_dict[field.field_id] = finding_target.portal_url

        # Add the severity field
        if self._severity_field:
            issue_dict[self._severity_field.field_id] = {
                'id': get_severity_level(self._severity_field, finding.severity)
            }

        return issue_dict
