from typing import Dict
from typing import List
from typing import Optional

from jira import Issue

from datatheorem_api_client import MobileApp
from datatheorem_api_client import SecurityFinding
from datatheorem_api_client import SecurityFindingTarget
from datatheorem_api_client import ResultsAPIClient
from datatheorem_api_client import StatusEnum

from dt_jira_integration.client import JipyApiClient
from dt_jira_integration.jira_configuration import JiraIntegrationConfiguration
from dt_jira_integration.jira_formatter import FindingTargetToJiraFormatter
from dt_jira_integration.logger import jira_logger
from dt_jira_integration.utils import JiraCommentTrackingTagEnum
from dt_jira_integration.utils import all_jira_integration_configs
from dt_jira_integration.utils import all_mobile_apps
from dt_jira_integration.utils import all_security_finding_targets
from dt_jira_integration.utils import all_security_findings
from dt_jira_integration.utils import does_have_dt_comment
from dt_jira_integration.utils import get_comments_from_issue
from dt_jira_integration.utils import is_config_valid


class CustomerJiraSyncAction:
    """The logic for running a DT/Jira synchronization for a full DT customer.
    """

    def __init__(self,
                 dt_api_key: str,
                 jira_config: JiraIntegrationConfiguration,
                 verify_jira_certificate: bool=True) -> None:
        self.jira_formatter = FindingTargetToJiraFormatter(jira_config)
        self.jira_global_config = jira_config
        self.results_client = ResultsAPIClient(dt_api_key)
        self.verify_jira_certificate = verify_jira_certificate

    def perform(self) -> None:
        jira_logger.info('Fetching all mobile apps')
        mobile_apps: Dict[int, MobileApp] = {app.id: app for app in all_mobile_apps(self.results_client)}

        jira_logger.info('Fetching all security findings')
        security_findings: Dict[int, SecurityFinding] = {
            finding.id: finding
            for finding in all_security_findings(self.results_client)
        }

        jira_logger.info('Fetching all security finding targets')
        security_finding_targets: List[SecurityFindingTarget] = all_security_finding_targets(self.results_client)

        jira_logger.info('Fetching all jira integration configs')
        jira_integrations: Dict[int, JiraIntegrationConfiguration] = {
            jira_integration.id: jira_integration
            for jira_integration in all_jira_integration_configs(self.results_client)
        }

        # No new issues have been updated (new/closed)
        if len(security_finding_targets) == 0:
            jira_logger.info('No issues have been found for syncing')
            return

        for target in security_finding_targets:
            security_finding: SecurityFinding = security_findings[target.security_finding_id]
            mobile_app: MobileApp = mobile_apps[target.mobile_app_id]

            # Return the correct jira config for app. If no specific config, then return global
            jira_config = jira_integrations.get(mobile_app.id, self.jira_global_config)

            if not is_config_valid(jira_config, security_finding, mobile_app):
                continue

            # NOTE (Gopar): Whenever we construct a jira api client, it adds a few seconds of latency
            # since it needs to check that credentials are correct, etc. Sadly there is no way of 'caching'
            # the configurations when retrieved from the api since each configuration is a completely different
            # from one another. Even though they might be exactly the same on the backend (All attributes are same),
            # they use different ID's. We could make a guess that if they have the same username and password then
            # it's the same config, but do we want to risk it? *insert thinking emoji*
            jira_client = JipyApiClient(
                jira_config,
                verify_jira_certificate=self.verify_jira_certificate,
            )

            action = FindingTargetJiraSyncAction(
                target,
                security_finding,
                mobile_app,
                self.jira_formatter,
                jira_client,
            )
            jira_issue = action.perform()

            if jira_issue:
                self.results_client.patch_security_finding_target(target.id, jira_issue.key)


class FindingTargetJiraSyncAction:
    """The logic for running a DT/Jira synchronization for a single finding target/ticket.
    """

    def __init__(
            self,
            finding_target: SecurityFindingTarget,
            finding: SecurityFinding,
            mobile_app: MobileApp,
            jira_formatter: FindingTargetToJiraFormatter,
            jira_client: JipyApiClient,
    ) -> None:
        self.finding_target = finding_target
        self.finding = finding
        self.mobile_app = mobile_app
        self.jira_formatter = jira_formatter
        self.jira_client = jira_client

    # We use Jira comments to tag the corresponding ticket, so we can keep track of the sync state
    _COMMENT_TARGET_FIXED = f'{JiraCommentTrackingTagEnum.TARGET_FIXED.value} - ' \
                            f'Data Theorem detected that this issue has been addressed in the latest scan.'

    _COMMENT_TARGET_RISK_ACCEPTED = f'{JiraCommentTrackingTagEnum.TARGET_IGNORED.value} - ' \
                                    f'This issue has been manually closed as "Risk Accepted" in the ' \
                                    f'Data Theorem portal.'

    _COMMENT_TARGET_COMP_CONTROL = f'{JiraCommentTrackingTagEnum.TARGET_IGNORED.value} - ' \
                                   f'This issue has been manually closed as "Compensating Control" in the ' \
                                   f'Data Theorem portal.'

    _COMMENT_TARGET_REOPENED = f'{JiraCommentTrackingTagEnum.TARGET_REOPENED.value} - ' \
                               f'This issue has been re-opened in the Data Theorem portal.'

    def perform(self) -> Optional[Issue]:
        """Return jira ticket if one was created.
        Otherwise return None if the ticket was simply updated.
        """
        # This target has *NOT* been saved to the customer's Jira instance
        if not self.finding_target.external_id:
            # should we create a ticket? Only if the issue status is OPEN or NEW
            if self.finding_target.current_status in [StatusEnum.OPEN, StatusEnum.NEW]:
                # Gather all the data needed to create a new Jira ticket
                jira_create_issue_dict = self.jira_formatter.get_create_issue_dict(
                    self.finding_target,
                    self.finding,
                    self.mobile_app.name,
                    self.mobile_app.platform
                )

                jira_logger.info(f'Creating issue with following properties: {jira_create_issue_dict}')
                jira_issue = self.jira_client._jira.create_issue(**jira_create_issue_dict)
                jira_logger.info(f'Saved ticket with finding id: {self.finding_target.id}, and '
                                 f'Jira issue key: {jira_issue.key}')
                return jira_issue

        # Target already exists in the customers Jira instance
        else:
            # Fetch the issue and its comments
            jira_issue = self.jira_client.get_issue(self.finding_target.external_id)
            comments = get_comments_from_issue(self.jira_client._config, jira_issue.key)

            # Look for DT's tags that would have been previously posted as comments
            has_target_fixed_tag = does_have_dt_comment(comments, JiraCommentTrackingTagEnum.TARGET_FIXED)
            has_target_ignored_tag = does_have_dt_comment(comments, JiraCommentTrackingTagEnum.TARGET_IGNORED)

            # If the target was fixed
            if self.finding_target.current_status in [StatusEnum.CLOSED_FIXED, StatusEnum.CLOSED_ITEM_NOT_FOUND]:
                # If the "fixed" tag hasn't been set on the Jira ticket yet, do it now
                # This will work even if the "ignored" tag (ie. customer accepted the risk) was used previously
                if not has_target_fixed_tag:
                    self.jira_client.add_comment(jira_issue, self._COMMENT_TARGET_FIXED)

            # If the target was manually closed in the portal
            if self.finding_target.current_status in [StatusEnum.CLOSED_RISK_ACCEPTED,
                                                      StatusEnum.CLOSED_COMPENSATING_CONTROL]:
                # If the "ignored" tag hasn't been set on the Jira ticket, do it now
                # but only if it has no "fixed" tag either
                if not has_target_ignored_tag and not has_target_fixed_tag:
                    if self.finding_target.current_status == StatusEnum.CLOSED_RISK_ACCEPTED:
                        new_comment = self._COMMENT_TARGET_RISK_ACCEPTED
                    else:
                        new_comment = self._COMMENT_TARGET_COMP_CONTROL
                    self.jira_client.add_comment(jira_issue, new_comment)

            # if the target was re-opened, it should have left a comment stating it was closed
            if (self.finding_target.current_status in [StatusEnum.OPEN, StatusEnum.NEW] and  # noqa: ignore
                (has_target_fixed_tag or has_target_ignored_tag)):
                self.jira_client.add_comment(jira_issue, self._COMMENT_TARGET_REOPENED)

        return None
