DT-Jira-Integration
============

Jira Integration Python Client. Test

Needs Python 3.6+ to work

## Install

```
pip install git+https://bitbucket.org/datatheorem/dt-jira-integration.git --process-dependency-links
```

## Example

Before being able to integrate, you must first have a Results Api Key. You can get your api key in the portal
by clicking on `API KEY` section in https://www.securetheorem.com/sdlc.

Once you obtain a Results API Key, you'll have two options on how you'd like to proceed.

The first is by explicitly passing the string, like below

```python
from dt_jira_integration.jira_configuration import JiraIntegrationConfiguration
from dt_jira_integration.sync_with_jira_action import CustomerJiraSyncAction

action = CustomerJiraSyncAction(
    'RESULTS_API_KEY',  # Manually passing the results api key
    JiraIntegrationConfiguration.from_yaml(yaml_path)
)
action.perform()
```

The second way is by saving the api key in an environment variable called `RESULTS_API_KEY` and using the libraries
helper function, `get_results_api_key`, to retrieve it.

```python
from dt_jira_integration.jira_configuration import JiraIntegrationConfiguration
from dt_jira_integration.config import get_results_api_key  # Helper function
from dt_jira_integration.sync_with_jira_action import CustomerJiraSyncAction

action = CustomerJiraSyncAction(get_results_api_key(), JiraIntegrationConfiguration.from_yaml(yaml_path))
action.perform()
```

After you decide, the next step will be to save your Jira configuration information in a yaml file.
It will look something like this:

```yaml
base_url: 'https://jira.com'
password: 'password'
username: 'username'
export_filter: 'ALL_ISSUES'
export_prod: True
export_pre_prod: False
type_of_issue_name: 'Security'
project_key_or_id: 'SEC'
severity_field_config:
  field_id: 'customefield_5321'
  high_severity_field_id: '1234'
  medium_severity_field_id: '0987'
  low_severity_field_id: '6542'
static_fields:
  - field_id: 'customefield_54321'
    field_value: 'some_val'
  - field_id: 'customefield_98762'
    field_value: 'another_val'
dynamic_fields:
  - field_id: 'customefield_46382'
    value: FINDING_PORTAL_URL
```

And can be read as:

```python
jira_config = JiraIntegrationConfiguration.from_yaml(path_to_file)

results_api_key = 'some_key'

action = CustomerJiraSyncAction(results_api_key, jira_config)
action.perform()
```

You can also construct your own Jira Configuration Object and pass that in instead of loading from yaml file if you'd like.

**One final** thing to note is that this will need to run daily, or whatever interval of time is desired to make sure you
have the latest status on all security issues. One way you can accomplish this by creating a cron job that runs daily, twice a week, etc.

**SSL Validation:** If you must disable SSL validation because your Jira instance has not been deployed with a valid certificate, you can provide the `verify_jira_certificate` flag when instantiating `CustomerJiraSyncAction`.
Doing so should be avoided as disabling SSL validation will make all the connections to your Jira instance easy to intercept and to decrypt.
Here is an example:

```python
action = CustomerJiraSyncAction(
    results_api_key,
    jira_config,
    # Explicitly disable ssl validation
    verify_jira_certificate=False
)
action.perform()
```


## Documentation

### CustomerJiraSyncAction

This class will take care of syncing issues between the Data Theorem Portal and your Jira instance,
the only two things it needs is the Results API Key, and a Jira Config Object (`JiraIntegrationConfiguration`)

After that, simply call the method `perform` to start the syncing process


### JiraIntegrationConfiguration

Contains all the necessary information to establish a connection to Jira. Here are the list of fields:

* `base_url`: str - URL to Jira Instance
* `username`: str - Account username Data Theorem will use to create issues under
* `password`: str - API Token for the account. Details to create an API Token can be found [here](https://confluence.atlassian.com/cloud/api-tokens-938839638.html?_ga=2.77675156.1411760106.1554771358-1563265542.1554771358)
* `export_filter`: str - What type of issues to export to Jira. Options are: `'ALL_ISSUES'`, `'P1_ISSUES_AND_BLOCKERS'`, and `'DISABLED'`. Defaults to `'P1_ISSUES_AND_BLOCKERS'`
* `export_prod`: bool - Whether to export issues only for Production apps
* `export_pre_prod`: bool - Whether to export issues only for Pre-Production apps
* `type_of_issue_name`: str - Issue name to be used, can be something like `Story`, or user defined value within Jira
* `project_key_or_id`: str - Ideally the key for the project that Data Theorem will create issues in
* `severity_field_config`: `SeverityFieldConfig` - (Optional) An object containing information on setting an issue as either High, Medium, or low priority.
* `static_fields`: List[`StaticField`] - (Optional) A list of pre-defined values to set when creating an issue. For example, can be used to on a field like `Source` that always needs to be set to "Data Theorem"
* `dynamic_fields`: List[`DynamicField`] - (Optional) A list of values that will not be known until synchronization starts.

### SeverityFieldConfig (Optional Jira Config Object)

Main purpose is to decided the priority of an issue (High, Medium, Low)

Jira admin will need to create a new custom field with three possible values in order to use this.

Contains a custom field ID, and three possible field values which map to High, Medium, and Low.

### StaticField (Optional Jira Config Object)

A set of mappings to be used when creating an issue in Jira. Requires a Field ID, and a Field Value

Field ID is needed to know which field should be created.
Field Value is needed to know what value should be created in that field.

ex:

```python
StaticField('customefield_12345', '12345')
```

In the above example, there is a field called `customfield_12345` than can have a value of `12345`. What `12345` means, is completely internal to customers Jira Instance. It could map to a string value, or something else


### DynamicField (Optional Jira Config Object)

Used for values which we will not know until synchronization starts.
An example of this is when Data Theorem creates an issue, we can provide the URL to that same issue in our portal. The URL
of that issue is not known until the sync process start, that's why we only need the Field ID to where to save it

```python
DynamicField('customefield_12345', JiraDynamicValueEnum.FINDING_PORTAL_URL)
```


## Dev Notes:

If you have the Google App Engine runtime installed and in your path. You'll want to remove it before
developing since it conflicts with the app

## Test

Run pytest

```
pytest
```
